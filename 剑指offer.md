# 3、从尾到头打印链表

解法一：使用ArrayList 中方法 add(index,value)，遍历链表将值插入到数组的首个位置

~~~java
public ArrayList<Integer> printListFromTailToHead(ListNode listNode) {
        ArrayList<Integer> list = new ArrayList<>();
        ListNode tmp = listNode;
        while(tmp!=null){
            list.add(0,tmp.val);
            tmp = tmp.next;
        }
        return list;
    }
~~~

解法二：使用递归方式，如果是null节点就什么都不做，不是null就先递归到最后一个节点然后将最后节点的值放入

```java
 	ArrayList<Integer> list = new ArrayList();//ArrayList需要放到外边
    public ArrayList<Integer> printListFromTailToHead(ListNode listNode) {
        if(listNode!=null){
            printListFromTailToHead(listNode.next);
            list.add(listNode.val);
        }
        return list;
    }
```

解法三、先将链表反转然后遍历放入ArrayList

# 20、定义栈的数据结构，请在该类型中实现一个能够得到栈的最小元素的 min 函数在该栈中，调用 top、min、push 及 pop 的时间复杂度都是 O(1)

说明：调用min与top只是查看最小的元素与栈顶元素，不会弹出元素

解法一：使用一个单向链表足够了，node节点包含当前链表的最小值，push时与当前链表的最小值进行比较，如果当前插入值不小于栈中的最小值，就给这个插入的node的min属性放入栈中当前最小值，==主要是利用了分段最小数与栈的先进后出的的特点==

~~~java
private Node head;

    public MinStack() {

    }

    public void push(int x) {

        if (head == null)
            head = new Node(x, x, null);
        else
            head = new Node(x, Math.min(head.min, x), head);
    }

    public void pop() {

        head = head.next;
    }

    public int top() {

        return head.val;
    }

    public int min() {

        return head.min;
    }

    private class Node {

        int val;
        int min;
        Node next;

        public Node(int val, int min, Node next) {

            this.val = val;
            this.min = min;
            this.next = next;
        }
    }
~~~

解法二：使用两个栈，一个栈专门来保存真实的数据，另一个栈专门用来保存当前的最小数，在push新元素时判断新元素是不是最小的，不是就将新元素放入到数据栈，并且最小数栈也再存放一次最小数栈的栈顶元素来保持和数据栈数据数量的相同，是最小的就将新元素的值入数据栈和最小数栈。==主要也是使用的分段最小数的记录特点。==

~~~java
class MinStack {
public:

    // 竟然是用栈来实现栈 ！！
    stack<int> sa;
    stack<int> sb;  // 辅助栈，栈顶存放目前栈sa中的最小元素

    MinStack() {
        
    }
    
    void push(int x) {
        sa.push(x);
        if(sb.empty() || x<sb.top())
            sb.push(x);
        else
            sb.push(sb.top());
    }
    
    void pop() {
        sa.pop();
        sb.pop();

    }
    
    int top() {
        return sa.top();
    }
    
    int min() {
        return sb.top();
    }
};

~~~

