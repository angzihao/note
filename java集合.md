# 一、集合

### 1. 集合框架架构

![](img/Snipaste_2021-02-19_21-43-37.png)

### 2. collection--list

![](img/Snipaste_2021-02-19_21-46-05.png)

#### ArrayList---LinkedList---Vector的异同

arraylist、linkedlist、vector三者都是存储的单列、有序、可重复的数据，前两个线程不安全。（都可以存储null）

+ arraylist（可以存储null）：底层是object类型的动态数组，jdk8中在new的时候创建一个空数组，在第一次add的时候才会创建一个长度为10的object类型数组，当数组不够使用时扩容为原来的1.5倍（创建时指定initialCapacity也是在此基础上扩容1.5）。对查询频繁的操作建议使用arraylist，因为使用的是数组具有索引（获取地址值根据索引加占用空间直接取地址内的数据）所以效率高。jdk7中在创建arraylist时候就创建了数组，类似于jdk7使用的饿汉式，jdk8使用的懒汉式的懒加载。

int newCapacity = oldCapacity + (oldCapacity >> 1),所以 ArrayList 每次扩容之后容量都会变为原来的 1.5 倍左右（oldCapacity为偶数就是1.5倍，否则是1.5倍左右）！ 奇偶不同，比如 ：10+10/2 = 15, 33+33/2=49。如果是奇数的话会丢掉小数.

+ linkedlist：底层采用node类型的双向链表结构，对于使用插入和删除频繁的操作建议使用linkedlist，比list的效率高。

+ vector：与arraylist相似，相比arraylist是线程安全的，并且扩容为原来的2倍。底层使用的synchronized保证了线程安全

### ArrayList扩容机制就是创建ArrayList的过程

#### ArrayList的删除过程

remove(Object o) 按照元素删除      如果元素是null 遍历数组移除第一个null，否则就遍历数组进行每个的equals删除第一个出现的元素

remove(Object o) 按照索引删除

~~~java
public E remove(int index) {
        rangeCheck(index);//检查传入的索引是否越界

        modCount++;//看下一个知识点
        E oldValue = elementData(index);

        int numMoved = size - index - 1;//计算需要移动的元素个数
        if (numMoved > 0)//大于0再移动
            System.arraycopy(elementData, index+1, elementData, index,
                             numMoved);
        elementData[--size] = null; // 最后将数组内的元素个数减一并置空

        return oldValue;
    }
~~~

#### ArrayList的modCount--

快速失败机制：就是遍历时候发现有线程进行修改了就立即遍历失败，抛出并发修改异常

List对象有一个成员变量modCount，它代表该List对象被修改的次数，每对List对象修改一次，modCount都会加1。主要就是用来在遍历时候进行的一个修改验证，在遍历时候是不能修改的否则ConcurrentModificationException并发修改异常，类似于乐观锁

Itr类里有一个成员变量expectedModCount（预期值），它的值为创建Itr对象的时候List的modCount值。用此变量来检验在迭代过程中List对象是否被修改了，如果被修改了则抛出java.util.ConcurrentModificationException异常。在每次调用Itr对象的next()方法的时候都会调用checkForComodification()方法进行一次检验，checkForComodification()方法中做的工作就是比较expectedModCount 和modCount的值是否相等，如果不相等，就认为还有其他对象正在对当前的List进行操作，那个就会抛出ConcurrentModificationException异常。

#### ArrayList常用方法--增删改查插遍历

![](img/arraylist.png)

### 3. collection---set

![Image](img\set.png)

HashSet、LinkedHashSet、TreeSet三者都是单列集合，用来无序、不可重复的对象，都是线程不安全的

1. HashSet底层是HashMap，map的key作为set，value存的是static final Object PRESENT = new Object()一个object的常量，在根据hashcode值判断元素位置时候，两个元素的位置重复的hash值不一定相等，比如3%2=1%2，当hashcode值不同时就可以直接放入对应的链表，在放入时jdk7是将新进来的元素放入到数组中链表的第一个元素，jdk8是放到链表的最后（可以理解为七上八下，上是数组上，下是链表下，最后）

2. object中的hashcode计算出来的值如果是基本数据类型（值相同）就都一样，引用数据类型（属性一样）没有重写HashCode方法就不一样，可以看做是对象地址，重写后是根据属性计算的hashcode，就会一样

3. 重写HashCode中的乘31,目的主要就是为了减少冲突，避免hashcode多次冲突后就会到一个桶里，就会导致该分散分布的数据却挤到一个桶中的链表上，这样会影响查询效率在，为什么是31呢为什么不是其他数字？

   + 因为31（2的5次方-1）占5个bits，int类型占32bits，如果过大会造成着溢出问题，过小起不到方法两个数据差异问题，比如比较2和3，如果都乘以2，那么差异就是2，都乘5，差异就是5
   + 并且31是一个素质数，就是只能被自己和1整除，这样也减少了hashcode冲突的概率，比如2\*4=8=1\*8就会造成hashcode冲突
   + 31可以由31 * i ==  (i << 5) - i（32倍的i减1倍的i就是31倍的i）来表示，现在很多虚拟机里面都有做相关优化，在底层计算的效率也会快一些

   ![](img/Snipaste_2021-02-20_15-45-04.png)

4. LinkedHashSet是HashSet的子类

   在HashSet的基础上LinkedHashSet额外维护了两个记录顺序的引用，记录数据添加的前后顺序，对于遍历频繁的操作，LinkedHashSet要优于HashSet

   ![](img/Snipaste_2021-02-20_16-32-45.png)

   

5. TreeSet底层使用的是红黑树（TreeMap），并且也是不能存放相同的数据，这个不是使用HashCode与equals进行判断的，是使用的compare与comparator方法进行排序和判断对象是否相等的，如果两个属性值相等就会返回0就是对象相等，添加失败。

   ![](img/Snipaste_2021-02-20_16-48-00.png)

### 4. Map

1. Map

   ![Image](img\mapFarmwork.png)

2. ![Image](img\map.png)


####  HashMap口述

HashMap是一个线程不安全的双列集合，键值对都可以存储null值，在创建HashMap时默认创建容量为16大小的数组，如果指定数组的大小也会向上转换成2的幂次方大小，在put元素时需要先使用hash方法计算出在数组中的位置，然后判断这个位置是否已经存在元素，如果存在就先判断hash是否相等，然后equals，在放入过程中如果链表长度超过8（并且数组长度超过64）就会扩容为原来容量的2倍或转换成红黑树。底层是数组+单向链表。

### hash冲突的解决办法

（1）线性探查法(Linear Probing)：（m是数组长度，d是数组中的位置）探查时从地址d开始，首先探查T[d]，然后依次探查T[d+1]，…，直到T[m-1]，此后又循环到T[0]，T[1]，…，直到探查到T[d-1]为止。

（2）线性补偿探测法：将线性探测的步长从 1 改为 Q ，即将上述算法中的 j ＝ (j ＋ 1) % m 改为： j ＝ (j ＋ Q) % m ，而且要求 Q 与 m 是互质的，以便能探测到哈希表中的所有单元。

（3）随机探测：就是生成一个随机的序列，在探测时依次使用生成的随机的序列中的随机数，

（4）拉链法：将hash冲突的对象使用单向链表存储

#### jdk7和jdk8的HashMap区别

+ jdk8创建后懒加载
+ jdk8是Node[]，jdk7是ENtry[]
+ jdk8是数组+链表+红黑树
+ jdk7在扩容后重新计算元素位置，jdk8是判断扩容后新增加的比特位对应的每个元素的hash值的比特位是否是1，是1就是原索引+原数组容量，否则就不动

#### 为什么必须是2的幂次方---计算位置和数据迁移

为了数据的均匀分布，减少hash冲突，因为hashmap是根据桶的大小-1&hash值【 (n - 1) & hash(key)】在模拟对hash求余运算%，如果桶的大小是2的n次幂，那么减一后的二进制全是1，&后的结果就是hash的后几位值，并且还不会超过桶的大小，如果不是2的n次方，那么减一后的二进制不全是1（比如最后以为是0）就不是模拟的对hash求余运算%，那么和hash&后的结果的最后一位永远都会是0，就会导致数组的一些位置永远不会插入数据，浪费数组的空间，加大hash冲突。

#### Hash为什么要右移16位异或

```
return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
```

因为数组位置的确定用的是与运算，仅仅最后几位有效，高位被丢失，如果桶的大小很小所以与&的位数就少，或者多个元素的hashcode值的高位变化很大，低位变化很小，那么就增加了hash冲突的概率，使用高16位（因为int占32位）异或低16位后，高16位的信息就被保留下来参与到位置计算中。如果采用&运算计算出来的值会向1靠拢，采用|运算计算出来的值会向0靠拢。

#### 为什么链表的长度为8是变成红黑树？为什么为6时又变成链表?

时间和空间的权衡，过长不利于查询，过短使用红黑树查询效率没有提升反而增加了占用空间，

为什么是8和6，使用泊松分布进行统计出的，就是使用的hash算法在理想情况下，链表元素符合泊松分布，一般不会达到8个节点，概率是很小的，但是元素位置是和key存储对象的hashcode有关的，而hashcode是需要用户来重写的，所以在用户使用的hashcode算法不够好时，可能会出现超过8的个数。



首先为什么要进行一个转换？为了防止链表过长，造成平均查找的时间复杂度是O（n）如果转换为红黑树，有自平衡的特点，可以防止不平衡的情况发生，始终可以将查找的时间复杂度控制在O（logn）,为什么要到8进行转换？一、因为单个TreeNode占用的空间是普通node的两倍，体现了时间和空间平衡的思想，最开始使用链表，空间占用是比较少的，而且由于链表短，所以查找的时间没有太大的问题，当链表越来越长的时候，需要用红黑树保证查询的效率，这个阈值是8，二、在理想的情况下，链表的长度符合泊松分布，当长度为8的时候，概率是很小的，通常Map里是不会存储这么的数据的，所以通常情况下，并不会发生链表向红黑树的转换，但是HashMap决定某一个元素落到那一个桶里，是和这个对象的hashcode有关，JDK并不能阻止用户实现自己的哈希算法，如果我们链表长度超过 8 就转为红黑树的设计，更多的是为了防止用户自己实现了不好的哈希算法时导致链表过长，从而导致查询效率低，而此时转为红黑树更多的是一种保底策略，用来保证极端情况下查询的效率。

####  JDK1.8HashMap扩容时做了哪些优化？

因此，我们在扩充HashMap的时候，不需要像JDK1.7的实现那样重新计算hash，只需要看看数组扩容后的数字对应的新的比特位与元素的hash值对应的比特位是1还是0就好了，是0的话索引没变，是1的话索引变成“原索引+数组原来的容量”，可以看看下图为16扩充为32的resize示意图.

![](img/Snipaste_2021-02-21_18-36-09.png)

#### HashMap、HashTable异同点

- HashTable线程安全，HashMap线程不安全；
- 初始值和扩容方式不同。HashTable的初始值为11，扩容为原大小的(oldCapacity << 1) + 1（2倍+1）。容量大小都采用奇数且为素数（任何数乘2+1后都是奇数和 素数），且采用取模法，这种方式散列更均匀。但有个缺点就是对素数取模的性能较低（涉及到除法运算），而Hashmap的长度都是2的次幂，
- HashMap的key、value都可为null，且value可多次为null，key多次为null时会覆盖，HashMap将存储的null键存储到数组的第1个元素。当HashTable的key、value都不可为null，否则直接报(NullPointException)专门做了判断。HashMap因为hash方法(key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);对null的key做了专门处理，get也是，所以可以保存null

#### HashMap为什么可以存储null的key和value，而HashTable不行

HashMap可以是因为在hash中判断如果为空就返回0来计算位置，HashTable不行是因为判断了key如果时null就报null异常

#### 加载因子为什么是0.75？

扩容因子太小数组的利用率会过低，导致频繁扩容而元素不多，太大也会导致链表过长而不易扩容，另一个原因是HashMap容量有一个固定要求，就是一定是2的幂，如果负载因子是0.75，可以和capacity的乘积的结果就可以是一个整数

#### ArrayList是满了才扩容hashmap为什么要提前扩容？

因为数组的元素可能一直都不会满，反而链表长度越来越长，导致查询效率下降

#### 为什么不是AVL树

首先AVL树与红黑树都是一种**自动平衡的二叉树**，AVL树的特点是左子树与右子树高度之差不能超过1，比如左子树节点是3，右子树最多是4，并且AVL树是牺牲插入性能与删除性能保证了查询的效率，因为需要保持树的平衡所以会旋转。红黑树可以看做是一种比较宽松的AVL树，红黑树牺牲了部分平衡性以换取插入/删除操作时少量的旋转操作，整体来说性能要优于AVL树  红黑树的平均统计性能优于AVL树。 

而红黑树是最长子树不超过最短子树的2倍，不能有连续两个的红色节点，每个路径上需要有相同的黑色节点个数，

AVL树 p平衡标准比较严格：每个左右子树的高度差不超过1 p最大高度是 1.44 ∗ log2 n + 2 − 1.328（100W个节点，AVL树最大树高28） 搜索、添加、删除都是 O(logn) 复杂度，其中添加仅需 O(1) 次旋转调整、删除最多需要 O(logn) 次旋转调整
 红黑树平衡标准比较宽松：没有一条路径会大于其他路径的2倍 最大高度是 2 ∗ log2(n + 1)（ 100W个节点，红黑树最大树高40）搜索、添加、删除都是 O(logn) 复杂度，其中添加、删除都仅需 O(1) 次旋转调整搜索的次数远远大于插入和删除，选择AVL树；搜索、插入、删除次数几乎差不多，选择红黑树相对于AVL树来说，红黑树牺牲了部分平衡性以换取插入/删除操作时少量的旋转操作，整体来说性能要优于AVL树  红黑树的平均统计性能优于AVL树，实际应用中更多选择使用红黑树。

#### 为什么不是二叉搜索树（右边关键字大于左边关键字）

因为二叉搜索树在最坏的插入情况下回退化成链表结构

#### HashMap如果key没有实现compareable接口，红黑树根据什么插入？

先根据类名，类名比较相等后根据地址值

1.  元素hash
2.  key和equals相等（就是元素重复，对value覆盖操作），不相等走3
3.  compare相等第4
4.  类名
5.  key地址

#### 遍历HashMap的四种方式

~~~java
//遍历的方式一：通过遍历map.entrySet(),或者通过迭代器的方式，因为foreach底层就是	//iterator
	//注意：这个Entry是java.util.Map包下面的
	写法一
	Iterator<Entry<String, Integer>> iterator = map.entrySet().iterator();
	while (iterator.hasNext()) {
		Entry<String, Integer> mapEntry = iterator.next();
		System.out.println(mapEntry.getKey() + " : " + mapEntry.getValue());
	}
	写法二
	for (Entry<String, Integer> set : map.entrySet()) {
		System.out.println(set.getKey() + " : " + set.getValue());
	}
	
	//遍历的方式二：分别遍历key和values
	for (String key : map.keySet()) {
		System.out.println("key: " + key);
	}
	for (Integer value : map.values()) {
		System.out.println("value: " + value);
	}
	
	//遍历方式三：JDK8以后使用Map接口中默认的方法
	map.forEach((key, value) -> {
		System.out.println(key + " : " + value);
	});
	
	//遍历方式四：通过get方法，不建议使用，因为迭代两次。keySet获取Iterator一次，还有通过get又迭代一次。降低性能。
	Set<String> set = map.keySet();
	for (String key : set) {
		System.out.println(key + " : " + map.get(key));
	}
}
~~~

### 使用红黑树的好处

按照原来的拉链法来解决冲突，，如果一个桶上的冲突很严重的话，是会导致哈希表的效率降低至 O（n），而通过红黑树的方式，可以把效率改进至 O（logn）。相比链式结构的节点，树型结构的节点会占用比较多的空间，所以这是一种以空间换时间的改进方式

### LinkedHashMap

LinkedHashMap是HashMap的子类，使用的Entry是对HashMap的Node在此包装，额外维护了前一个和后一个的指针，所以遍历会有顺序

![](img/Snipaste_2021-02-21_10-16-35.png)

##  扩容时导致死循环

# TODO----Hashtable

ConcurrentHashMap的key和value不能为null，

元素的hash值的高（以segment中的table长度有关）作为计算segment的位置，低位计算在segment中的table的位置

# ConcurrentHashMap

在jdk7使用Segment 分段锁，使用的ReentrantLock 进行加锁

jdk8使用 `CAS + synchronized`先使用CAS尝试直接放入（数组元素下的链表为空的情况），如果已经存在链表就是用synchronized`进行加锁添加

## 为什么不用ReentrantLock而用synchronized ?

- 减少内存开销:如果使用ReentrantLock则需要节点继承AQS来获得同步支持，增加内存开销，而1.8中只有头节点需要进行同步。
- 内部优化:synchronized则是JVM直接支持的，JVM能够在运行时作出相应的优化措施：锁粗化、锁消除、锁自旋等等。
- 通过源码可以看出 使用 CAS + synchronized 方式时 加锁的对象是每个链条的头结点，也就是 锁定 的是冲突的链表，所以再次提高了并发度，并发度等于链表的条数或者说 桶的数量。那为什么sement 不把段的大小设置为一个桶呢，因为在粒度比较小的情况下，如果使用ReentrantLock则需要节点继承AQS来获得同步支持，增加内存开销，而1.8中只有头节点需要进行同步，粒度表较小，相对来说内存开销就比较大。所以不把segment的大小设置为一个桶。

# 位图



1. TreeMap与Hashtable都不能存储null的key和value

   HashMap和LinkedHahsMap可以存储null的key和value

   

2. HashMap是一个线程不安全的，put 方法比较复杂，实现步骤大致如下：

   1、先通过 hash 值计算出 key 映射到哪个桶。

   2、如果桶上没有碰撞冲突，则直接插入。

   3、如果出现碰撞冲突了，则需要处理冲突：

   （1）如果该桶使用红黑树处理冲突，则调用红黑树的方法插入。

   （2）否则采用传统的链式方法插入。如果链的长度到达临界值，则把链转变为红黑树。

   4、如果桶中存在重复的键，则为该键替换新值。

   5、如果 size 大于阈值，则进行扩容

3. HashMap使用的hash（hash（）方法）值是根据重写的hashcode值计算（就是再次进行加工）的

4. HashMap的自定义类型key需要重写hashcode与equals，value需要重写equals，因为在使用containsValue（）判断value是否存在需要使用。

5. hashmap的元素的key如果重复会将value值覆盖，区别于set的是单列只有key重复是添加失败，map是覆盖value，put方法如果添加的元素不重复则返回null值，如果添加的元素重复则覆盖旧的value并返回。

6. 创建HashMap时如果指定数组大小，加入指定为15，最后创建的还是16（会经过运算数组容量必须是2的幂），为什么？主要是为了计算元素的位置使用

7. 求元素在数组中的位置公式为，hashcode&（length-1），&操作只有都是1结果才是1，因为数组的length是2的幂，所以-1后低位就都是0，经过&运算后计算出的值不会大于length-1值，hashcode中比length值高位的值都会变0，再与length&操作最大的结果就是length-1

   hashcode值是使用的HashMap中的hash（）方法计算的，采用了二次哈希的方式，其中 key 的 hashCode 方法是一个native 方法：

   ~~~java
   static final int hash(Object key) {
    int h;
    return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>>16);
       这个 hash 方法先通过 key 的 hashCode 方法获取一个哈希值，再拿这个哈希值与它的高 16 位的哈希值做一个异或操作来得到最后的哈希值
   ~~~

   如果当 n （数组长度）很小，假设为 64 的话，那么 n-1即为 63（0x111111），这样的值跟 hashCode()直接做与操作，实际上只使用了哈希值的后 6 位。如果当哈希值的高位变化很大，低位变化很小，这样就很容易造成冲突了，所以这里把高低位都利用起来，从而解决了这个问题。相当于异或高位后将高位也参与到hashcode的计算中。

   ## TODO 如果数组长度不是2的幂会怎么样

8. 当添加的元素需要添加的位置不为空（为空达到临界值也不扩容），且达到临界值（容量*加载因子0.75--16\*0.75=12）才会扩容为原来的2倍。扩容之后所有的元素会重新计算位置，所以扩容也是很消耗内存的

9. ArrayList是满了才扩容hashmap为什么要提前扩容？因为数组的元素可能一直都不会满，反而链表长度越来越长，导致查询效率下降，为什么是0.75？扩容因子太小数组的利用率会过低，导致频繁扩容而元素不多，太大返回也会链表过长而不易扩容

10. HashMap的链表是单向链表

    

    LinkedHashMap是HashMap的子类，使用的Entry是对HashMap的Node在此包装，额外维护了前一个和后一个的指针，所以遍历会有顺序

![](img/Snipaste_2021-02-21_10-16-35.png)

13. HashTable的区别---默认初始容量是11，不需要是2的幂 (hash & 0x7FFFFFFF) % tab.length直接&全1，hashmap是将数组长度全置1，增加容量时，每次将容量变为"原始容量x2 + 1"

    






