# 一、Jdk与Jre

![](img\image(4).png)

# 二、异常

​	![Image](img/error.png)

​	Throwable是异常的父类，子类有Error和Exception，Error是jvm虚拟机报出的错误比如OOM，Exception又分为受检异常和运行时异常，受检异常需要使用try-catch捕获或者throws抛出，运行时异常，也可以使用try-catch捕获处理，比如3/0就是运行时异常。

如何自定义异常？

实现现有的Exception或RuntimeException，提供全局常量serialVersionUID序列化id，提供构重载造器

其中自定义Exception在使用时需要手动处理，自定义RuntimeException不需要手动处理

# 三、枚举类

枚举类总结：

+ 是一种特殊的类，枚举类中的属性都是static final的枚举类类型的对象
+ 枚举类可以实现接口但是不能继承类
+ 枚举最大的特点是私有的构造器

枚举类的吗，自带方法有values（返回枚举类中所有对象的数组），返回值是数组

valueOf（string name）将字符串转换成枚举类就=MyEnum.HELLO（枚举类对象）就是获取枚举类对象（不是创建）

# 四、File类常用类

![](img/file.png)

![](img/file2.png)

![](img/file3.png)

# 五、反射

![](img/fanshe.png)

```java
System.out.println(MyTest3.class);
//使用对象的实例getClass
System.out.println(new MyTest3().getClass());
//使用Class.forName加载
System.out.println(Class.forName("MyTest3"));
```

# 六、线程

多线程之间的通信如何进行？使用wait和notify或者await和singal

integer语法糖的底层是拆箱是valueOf装箱intvalue

# 七、io

按操作单位分为：字节流、字符流

按流向不同分为：输入流、输出流

按角色分为：节点流、处理流

# 八、clone

**原型模式实际上就是从一个对象再创建另外一个可定制的对象，而且不需要知道任何创建的细节。在初始化的信息不发生变化的情况下，克隆是最好的办法，既隐藏了对象创建的细节，又大大提高了性能。object的clone方法调用的时本地方法进行二进制克隆，比new效率要高。**

直接使用suppper.clone()不能实现拷贝，需要手动实现重写手动new

```java
@Override
protected Object clone() throws CloneNotSupportedException {
    return new Test4_12();
}
```

深拷贝浅拷贝：区别在于对象中的引用数据类型，如果浅拷贝则是直接拷贝的引用对象的地址，深拷贝拷贝的是一个对象，

实现深拷贝的方式：如果对象的引用数据类型比较深可以使用序列化与反序列化，如果比较浅可以使用重写clone方法
序列化：https://www.cnblogs.com/xdp-gacl/p/3777987.html

# **克隆会破坏单例模式**
1、某一个对象直接调用clone方法，会抛出异常，并不能成功克隆一个对象。调用该方法时，必须实现一个Cloneable 接口。这也就是原型模式的实现方式。还有即若是该类实现了cloneable接口，尽管构造函数是私有的，他也能够建立一个对象。即clone方法是不会调用构造函数的，他是直接从内存中copy内存区域的。因此单例模式的类是不能够实现cloneable接口的。

2、序列化可能对单例模式的破坏code

一是能够实现数据的持久化；二是能够对象数据的远程传输。 
若是过该类implements Serializable，那么就会在反序列化的过程当中再创一个对象。这个问题的解决办法就是在反序列化时，指定反序化的对象实例。添加以下方法：对象
```java
private static final long serialVersionUID = -3979059770681747301L;

    private volatile static Singleton singleton;

    private Object readResolve() {
        return singleton;
    }
```
3、反射可能对单例模式的破坏
反射是能够获取类的构造函数，再加一行 setAccessible(true);就能够调用私有的构造函数，建立对象了。那么防止反射破坏Java单例模式的方法就是：当第二次调用构造函数时抛出异常。代码以下：接口
```java
private volatile static Singleton1 singleton;

    private  static boolean  flag = true;
    private Singleton1 (){
        if(flag){
        flag = false;   
        }else{
            throw new RuntimeException("单例模式险些被破坏，第二个对象未建立成功");
        }

    }
```




 AVL树 p平衡标准比较严格：每个左右子树的高度差不超过1 p最大高度是 1.44 ∗ log2 n + 2 − 1.328（100W个节点，AVL树最大树高28） p搜索、添加、删除都是 O(logn) 复杂度，其中添加仅需 O(1) 次旋转调整、删除最多需要 O(logn) 次旋转调整
 红黑树
p平衡标准比较宽松：没有一条路径会大于其他路径的2倍 p最大高度是 2 ∗ log2(n + 1)（ 100W个节点，红黑树最大树高40） p搜索、添加、删除都是 O(logn) 复杂度，其中添加、删除都仅需 O(1) 次旋转调整
 搜索的次数远远大于插入和删除，选择AVL树；搜索、插入、删除次数几乎差不多，选择红黑树
 相对于AVL树来说，红黑树牺牲了部分平衡性以换取插入/删除操作时少量的旋转操作，整体来说性能要优于AVL树  红黑树的平均统计性能优于AVL树，实际应用中更多选择使用红黑树。