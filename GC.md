# 一、垃圾回收概述

### 什么是垃圾？

就是一些没有用的对象，在程序中没有任何指针指向的对象，如果不对垃圾进行回收，就会导致垃圾越来越多而出现内存溢出。

### 大厂面试题

#### 蚂蚁金服

- 你知道哪几种垃圾回收器，各自的优缺点，重点讲一下cms和G1？
- JVM GC算法有哪些，目前的JDK版本采用什么回收算法？
- G1回收器讲下回收过程GC是什么？为什么要有GC？
- GC的两种判定方法？CMS收集器与G1收集器的特点

#### 百度

- 说一下GC算法，分代回收说下

- 垃圾收集策略和算法

  # 是基于分代回收的分代策略 ，jvm在每一次执行垃圾收集器时，只是对一小部分内存

#### 天猫

- JVM GC原理，JVM怎么回收内存
- CMS特点，垃圾回收算法有哪些？各自的优缺点，他们共同的缺点是什么？

#### 滴滴

Java的垃圾回收器都有哪些，说下g1的应用场景，平时你是如何搭配使用垃圾回收器的

#### 京东

- 你知道哪几种垃圾收集器，各自的优缺点，重点讲下cms和G1，
- 包括原理，流程，优缺点。垃圾回收算法的实现原理

#### 阿里

- 讲一讲垃圾回收算法。

- 什么情况下触发垃圾回收？

  eden分配对象失败、

  old1.年老代被写满
  持久代（Perm）被写满
  System.gc()被显示调用
  上一次GC之后Heap的各域分配策略动态变化

- 如何选择合适的垃圾收集算法？

  

- JVM有哪三种垃圾回收器？

#### 字节跳动

- 常见的垃圾回收器算法有哪些，各有什么优劣？
- System.gc（）和Runtime.gc（）会做什么事情？
- Java GC机制？GC Roots有哪些？
- Java对象的回收方式，回收算法。
- CMS和G1了解么，CMS解决什么问题，说一下回收的过程。
- CMS回收停顿了几次，为什么要停顿两次?

## 为什么需要GC

1. 需要清理垃圾来释放内存空间
2. 整理碎片内存空间，方便创建对象。碎片整理将所占用的堆内存移到堆的一端，以便JVM将整理出的内存分配给新的对象。



## Java垃圾自动回收机制

### 优点

自动内存管理，无需开发人员手动参与内存的分配与回收，这样降低内存泄漏和内存溢出的风险

没有垃圾回收器，java也会和cpp一样，各种悬垂指针，野指针，泄露问题让你头疼不已。

自动内存管理机制，将程序员从繁重的内存管理中释放出来，可以更专心地专注于业务开发

### GC主要关注的区域

GC主要关注于 方法区 和堆中的垃圾收集

![](img/image-20200712092427246.png)

垃圾收集器可以对年轻代回收，也可以对老年代回收，甚至是全栈和方法区的回收

- 其中，Java堆是垃圾收集器的工作重点

从次数上讲：

- 频繁收集Young区
- 较少收集Old区
- 基本不收集Perm区（元空间）

# 二、垃圾回收算法

## 1、标记阶段：

在GC执行垃圾回收之前，首先区分出内存中的存活对象与死亡对象，只有被标记为己经死亡的对象，GC才会在执行垃圾回收时，释放掉其所占用的内存空间，因此这个过程我们可以称为垃圾标记阶段。

### 引用计数算法

那么在JVM中究竟是如何标记一个死亡对象呢？简单来说，当一个对象已经不再被任何的存活对象继续引用时，就可以宣判为已经死亡。

判断对象存活一般有两种方式：**引用计数算法**和**可达性分析算法。**

引用计数算法（Reference Counting），就是每个对象保存一个引用计数器。当有被对象引用就加一，释放就减一，当计数器为0就是没有引用了

优点：实现简单，垃圾对象便于辨识；判定效率高，回收没有延迟性。

缺点：需要单独的字段存储计数器增加了存储空间的开销。每次赋值都需要更新计数器增加了时间开销。<span style="color:red">无法处理循环引用的情况</span>

#### 引用计数算法小结

引用计数算法，是很多语言的资源回收选择，例如因人工智能而更加火热的Python，它更是同时支持引用计数和垃圾收集机制。

具体哪种最优是要看场景的，业界有大规模实践中仅保留引用计数机制，以提高吞吐量的尝试。

Java并没有选择引用计数，是因为其存在一个基本的难题，也就是很难处理循环引用关系。Python如何解决循环引用？

>手动解除：很好理解，就是在合适的时机，解除引用关系。
>使用弱引用weakref，weakref是Python提供的标准库，旨在解决循环引用。

### 可达性分析算法

可达性分析算法：也可以称为 根搜索算法、追踪性垃圾收集

该算法可以有效地解决在引用计数算法中循环引用的问题，防止内存泄漏的发生。

Java、C#选择的可达性分析。

#### 概念

所谓"GCRoots”根集合就是一组必须活跃的引用。

基本思路：

- 可达性分析算法是以根对象集合（GCRoots）为起始点，按照从上至下的方式搜索被根对象集合所连接的目标对象是否可达。

- 使用可达性分析算法后，内存中的存活对象都会被根对象集合直接或间接连接着，搜索所走过的路径称为引用链

- 如果目标对象没有任何引用链相连，则是不可达的，就意味着该对象己经死亡，可以标记为垃圾对象。

  

### GC Roots可以是哪些？

<span style="color:red;">本地方法栈+虚拟机栈+方法区的常量池和静态变量+锁对象+可能会在回收年轻代时老年代中的对象</span>

- 虚拟机栈中引用的对象
  - 比如：各个线程被调用的方法中使用到的参数、局部变量等。
- 本地方法栈内JNI（通常说的本地方法）引用的对象
- 方法区中类静态属性引用的对象（在jdk7时和string table一起放入堆中）
  - 比如：Java类的引用类型静态变量
- 方法区中常量引用的对象
  - 比如：字符串常量池（string Table）里的引用（字符串常量池可能存放的是对象地址）
- 所有被同步锁synchronized持有的对象
- Java虚拟机内部的引用。
  - 基本数据类型对应的Class对象，一些常驻的异常对象（如：Nu11PointerException、outofMemoryError），系统类加载器。
- 反映java虚拟机内部情况的JMXBean、JVMTI中注册的回调、本地代码缓存等。
- 临时性的对象：如果单独对年轻代回收，那么老年代中的对象也会放入GCRoot集合

##### 小技巧

如果一个指针，它保存了堆内存里面的对象，但是自己又不存放在堆内存里面，那它就是一个Root。

### 注意

就是在使用可达性分析时需要暂停用户线程，否则就可能程序在不断的释放引用，而可达性分析也一直在标记。

如果要使用可达性分析算法来判断内存是否可回收，那么分析工作必须在一个能保障一致性的快照中进行。这点不满足的话分析结果的准确性就无法保证。

这点也是导致GC进行时必须“stop The World”的一个重要原因。即使是号称（几乎）不会发生停顿的CMS收集器中，枚举根节点时也是必须要停顿的。

## 2、清除阶段

当成功区分出内存中存活对象和死亡对象后，GC接下来的任务就是执行垃圾回收，释放掉无用对象所占用的内存空间，以便有足够的可用内存空间为新对象分配内存。目前在JVM中比较常见的三种垃圾收集算法是

- 标记一清除算法（Mark-Sweep）
- 复制算法（copying）
- 标记-压缩算法（Mark-Compact）

标记-清除算法（Mark-Sweep）是一种非常基础和常见的垃圾收集算法，该算法被J.McCarthy等人在1960年提出并并应用于Lisp语言。

### 标记-清除算法：

当堆中的有效内存空间（available memory）被耗尽的时候，就会停止整个程序（也被称为stop the world），然后进行两项工作，第一项则是标记，第二项则是清除

- **标记**：Collector从引用根节点开始遍历，**标记所有被引用的对象**。一般是在对象的Header中记录为可达对象。
  - **标记的是引用的对象，不是垃圾！！**
- **清除**：Collector对堆内存从头到尾进行线性的遍历，如果发现某个对象在其Header中没有标记为可达对象，则将其回收

#### 什么是清除？

这里所谓的清除并不是真的置空，而是把需要清除的对象地址保存在空闲的地址列表里。下次有新对象需要加载时，判断垃圾的位置空间是否够，如果够，就存放覆盖原有的地址。

关于空闲列表是在为对象分配内存的时候 提过

- 如果内存规整
  - 采用指针碰撞的方式进行内存分配
- 如果内存不规整
  - 虚拟机需要维护一个列表
  - 空闲列表分配，在分配的时候从列表中找到一块足够大的空间划分给对象实例

#### 缺点

- 标记清除算法的效率不算高
- 在进行GC的时候，需要停止整个应用程序，用户体验较差
- 这种方式清理出来的空闲内存是不连续的，产生内碎片，需要维护一个空闲列表

### 复制算法：

总结：将内存分为两块，每次只使用一块，在垃圾回收时将需要垃圾回收的的一块内存中存活的对象赋值到另一块内存中，然后将这一块内存置空，最后交换两个内存的角色。

将活着的内存空间分为两块，每次只使用其中一块，在垃圾回收时将正在使用的内存中的存活对象复制到未被使用的内存块中，之后清除正在使用的内存块中的所有对象，交换两个内存的角色，最后完成垃圾回收

#### 优点

- 在复制的过程中保证了内存的连续性，不会出现“碎片”问题。

#### 缺点

- 需要两倍的内存空间，造成内存浪费。
- 赋值过程中需要需要维护对象之间的引用，并且浪费cpu的资源和时间
- 如果存活的对象少还行，如果存活的对象过多就会造成效率低下

#### 注意

copy算法会STW，但是因为对象比较少，所以也没有多大的影响。

如果系统中的垃圾对象很多，复制算法需要复制的存活对象数量并不会太大，或者说非常低才行（老年代大量的对象存活，那么复制的对象将会有很多，效率会很低）

在新生代，对常规应用的垃圾回收，一次通常可以回收70% - 99% 的内存空间。回收性价比很高。所以现在的商业虚拟机都是用这种收集算法回收新生代。

### 标记-整理算法(标记压缩)

第一阶段和标记清除算法一样，从根节点开始标记所有被引用对象

第二阶段将所有的存活对象压缩到内存的一端，按顺序排放。之后，清理边界外所有的空间。

#### 标清和标整的区别

标记-压缩算法，也可以把它称为标记-清除-压缩（Mark-Sweep-Compact）算法。

本质的区别就是标记整理在标记后会将存活的对象按照内存地址依次排列的整理，并且清除其他空间的垃圾，而标记清除只是标记存活对象后维护一个空闲列表，所以标记整理也不会维护一个空闲的列表，只需要记录内存的起始地址就可以了。

#### 优点

- 消除了标记-清除算法当中，内存区域分散的缺点，我们需要给新对象分配内存时，JVM只需要持有一个内存的起始地址即可。
- 消除了复制算法当中，内存减半的高额代价。

#### 缺点

- 从效率上来说，标记-整理算法要低于复制算法。
- 也需要维护对象间的引用关系：移动对象的同时，如果对象被其他对象引用，则还需要调整引用的地址
- 移动过程中，需要全程暂停用户应用程序。即：STW

## 小结

效率上来说，复制算法是当之无愧的老大，但是却浪费了太多内存。

而为了尽量兼顾上面提到的三个指标，标记-整理算法相对来说更平滑一些，但是效率上不尽如人意，它比复制算法多了一个标记的阶段，比标记-清除多了一个整理内存的阶段。

|              | 标记清除           | 标记整理         | 复制                                  |
| ------------ | ------------------ | ---------------- | ------------------------------------- |
| **速率**     | 中等               | 最慢             | 最快                                  |
| **空间开销** | 少（但会堆积碎片） | 少（不堆积碎片） | 通常需要活对象的2倍空间（不堆积碎片） |
| **移动对象** | 否                 | 是               | 是                                    |

综合我们可以找到，没有最好的算法，只有最合适的算法

### 分代收集算法

分代收集并不是一种垃圾回收的某一种具体的算法，而是一种分代回收垃圾的思想或方案。就是根据堆空间中对象不同的生命周期来使用不同的收集算法，对于频繁收集的分区比如新生代使用高效的复制算法，而复制算法内存利用率不高的问题，通过两个survivor的设计得到缓解。对于空间比较大的空间比如老年代使用标记清除或标记整理。

目前几乎所有的GC都采用分代收集算法执行垃圾回收的

在HotSpot中，基于分代的概念，GC所使用的内存回收算法必须结合年轻代和老年代各自的特点。

- 年轻代（Young Gen）

年轻代特点：区域相对老年代较小，对象生命周期短、存活率低，回收频繁。

这种情况复制算法的回收整理，速度是最快的。复制算法的效率只和当前存活对象大小有关，因此很适用于年轻代的回收。而复制算法内存利用率不高的问题，通过hotspot中的两个survivor的设计得到缓解。

- 老年代（Tenured Gen）

老年代特点：区域较大，对象生命周期长、存活率高，回收不及年轻代频繁。

这种情况存在大量存活率高的对象，复制算法明显变得不合适。一般是由标记-清除或者是标记-清除与标记-整理的混合实现。

- Mark标记阶段的开销与存活对象的数量成正比。
- Sweep清除阶段的开销与所管理区域的大小成正相关。
- compact整理阶段的开销与存活对象的数据成正比。

以HotSpot中的CMS回收器为例，CMS是基于Mark标记-Sweep清除实现的，对于对象的回收效率很高。而对于碎片问题，CMS采用基于Mark标记整理-Compact整理算法的Serial old回收器作为补偿措施：当内存回收不佳（碎片导致的Concurrent Mode Failure时），将采用serial old执行FullGC以达到对老年代内存的整理。

分代的思想被现有的虚拟机广泛使用。几乎所有的垃圾回收器都区分新生代和老年代

### 增量收集算法

增量收集算法就是将垃圾收集线程与用户线程交替执行，每次只收集一小片区域，让用户看起来并没有停顿，缺点就是增加了线程上下文的切换的消耗，影响了系统整体的吞吐量。

总的来说，增量收集算法的基础仍是传统的标记-清除和复制算法。**增量收集算法通过对线程间冲突的妥善处理**，允许垃圾收集线程以分阶段的方式完成标记、清理或复制工作

### 分区算法

分区算法和增量收集算法类似，都是将一个大的垃圾回收任务分割为多个小的任务，每次都是处理一个或几个任务，而增量收集是根据用户线程停顿时间来划分任务，分区是根据内存大小来进行分块收集，分块使用。

分代算法将按照对象的生命周期长短划分成两个部分，分区算法将整个堆空间划分成连续的不同小区间。每一个小区间都独立使用，独立回收。这种算法的好处是可以控制一次回收多少个小区间。

# 三、对象的finalization机制

就是重写Object中的finalize（）方法来控制对象被销毁之前的自定义处理逻辑。就是垃圾回收此对象之前，会先调用这个对象的finalize()方法。通常在这个方法中进行一些资源释放和清理的工作，比如关闭文件、套接字和数据库连接等。

### 注意

永远不要主动调用某个对象的finalize（）方法应该交给垃圾回收机制调用。理由包括下面三点：

- 在finalize（）时可能会导致对象复活。
- finalize（）方法的执行时间是没有保障的，它完全由Gc线程决定，极端情况下，若不发生GC，则finalize（）方法将没有执行机会。
  - 因为优先级比较低，即使主动调用该方法，也不会因此就直接进行回收
- 一个糟糕的finalize（）会严重影响Gc的性能。

从功能上来说，finalize（）方法与c++中的析构函数比较相似，但是Java采用的是基于垃圾回收器的自动内存管理机制，所以finalize（）方法在本质上不同于C++中的析构函数。

由于finalize（）方法的存在，虚拟机中的对象一般处于三种可能的状态。

### 生存还是死亡？

如果从所有的根节点都无法访问到某个对象，说明对象己经不再使用了。一般来说，此对象需要被回收。但事实上，也并非是“非死不可”的，这时候它们暂时处于“缓刑”阶段。**一个无法触及的对象有可能在某一个条件下“复活”自己**，如果这样，那么对它的回收就是不合理的，为此，定义虚拟机中的对象可能的三种状态。如下：

- 可触及的：从根节点开始，可以到达这个对象。
- 可复活的：对象的所有引用都被释放，但是对象有可能在finalize（）中复活。
- 不可触及的：对象的finalize（）被调用，并且没有复活，那么就会进入不可触及状态。不可触及的对象不可能被复活，因为**finalize()只会被调用一次**。

以上3种状态中，是由于finalize（）方法的存在，进行的区分。只有在对象不可触及时才可以被回收。

### 具体过程

判定一个对象objA是否可回收，至少要经历两次标记过程：

- 如果对象objA到GC Roots没有引用链，则进行第一次标记。

- 进行筛选，判断此对象是否有必要执行finalize（）方法
  - 如果对象objA没有重写finalize（）方法，或者finalize（）方法已经被虚拟机调用过，则虚拟机视为“没有必要执行”，objA被判定为不可触及的。
  - 如果对象objA重写了finalize（）方法，且还未执行过，那么objA会被插入到F-Queue队列中，由一个虚拟机自动创建的、低优先级的Finalizer线程触发其finalize（）方法执行。
  - finalize（）方法是对象逃脱死亡的最后机会，稍后GC会对F-Queue队列中的对象进行第二次标记。如果objA在finalize（）方法中与引用链上的任何一个对象建立了联系，那么在第二次标记时，objA会被移出“即将回收”集合。之后，对象会再次出现没有引用存在的情况。在这个情况下，finalize方法不会被再次调用，对象会直接变成不可触及的状态，也就是说，一个对象的finalize方法只会被调用一次。

# 四、JProfiler的GC Roots溯源

我们在实际的开发中，一般不会查找全部的GC Roots，可能只是查找某个对象的整个链路，或者称为GC Roots溯源，这个时候，我们就可以使用JProfiler

### 排查OOM

首先设置启动参数在OOM时生成dump快照，当出现错误后可以使用JProfiler查看当前存在的对象，如果有对象不应该存在而没有被回收，就是出现了内存泄漏，然后我们可以查看这个对象的GCRoot引用链区查看对象在什么地方被引用了，然后去修改代码。如果是一个应该存在的对象因为太大而爆出的内存溢出，那么就应该调整堆的内存大小或将大的对象进行优化。

# 五 垃圾回收相关概念

## System.gc()的理解

通过system.gc（）或者Runtime.getRuntime().gc() 的调用，可能会触发FullGC，同时对老年代和新生代进行回收，尝试释放被丢弃对象占用的内存。不一定会百分百触发，system.gc（）底层调用的是Runtime.getRuntime().gc() ，但是不建议手动调用，因为垃圾是自动回收的，不应该来手动回收。<span style="color:red">调用System.runFinalization()（需要先调用system.gc）</span>

## 内存溢出

如果应用程序占用的内存增长速度非常快，造成垃圾回收已经跟不上内存消耗的速度，就会容易出现ooM的情况。

javadoc中对outofMemoryError的解释是，没有空闲内存，并且垃圾收集器也无法提供更多内存。

导致内存溢出的可能原因：

+ 堆内存设置不够。
+ 可能存在内存泄漏问题；
+ 大量一些大的对象引用时间过长，不能及时被回收

在抛出OutofMemoryError之前，通常垃圾收集器会被触发，尽其所能去清理出空间。

也不是在任何情况下垃圾收集器都会被触发的，比如，我们去分配一个超大对象，类似一个超大数组超过堆的最大值，JVM可以判断出垃圾收集并不能解决这个问题，所以直接抛出OutofMemoryError。

## 内存泄漏

也称作“存储渗漏”。对象不会再被程序用到了，但是GC又不能回收他们的情况，才叫内存泄漏。或疏忽对象的释放会导致对象的生命周期变得很长甚至导致00M。

内存泄漏并不会立刻引起程序崩溃，但是一旦发生内存泄漏，程序中的可用内存就会被逐步蚕食，直至耗尽所有内存，最终出现outofMemory异常，导致程序崩溃。

Java使用可达性分析算法，最上面的数据不可达，就是需要被回收的。后期有一些对象不用了，按道理应该断开引用，但是存在一些链没有断开，从而导致没有办法被回收。

#### 举例

- 单例模式

因为是单例的生命周期和应用程序是一样长的，如果单例对象持有对外部对象的引用的话，那么这个外部对象是不能被回收的，则会导致内存泄漏的产生。

- 一些提供close的资源未关闭导致内存泄漏

数据库连接（dataSourse.getConnection() ），网络连接（socket）和io连接必须手动close，否则是不能被回收的。

## Stop The World

 stop-the-world，简称STw，指的是GC事件发生过程中，会产生应用程序的停顿。停顿产生时整个应用程序线程都会被暂停，没有任何响应，有点像卡死的感觉，这个停顿称为STW。

可达性分析算法中枚举根节点（GC Roots）会导致所有Java执行线程停顿。

- 分析工作必须在一个能确保一致性的快照中进行
- 一致性指整个分析期间整个执行系统看起来像被冻结在某个时间点上

- 如果出现分析过程中对象引用关系还在不断变化，则分析结果的准确性无法保证

STW事件和采用哪款GC无关所有的GC都有这个事件。

哪怕是G1也不能完全避免Stop-the-world情况发生，只能说垃圾回收器越来越优秀，回收效率越来越高，尽可能地缩短了暂停时间。

STW是JVM在后台自动发起和自动完成的。在用户不可见的情况下，把用户正常的工作线程全部停掉。

开发中不要用system.gc() 会导致stop-the-world的发生。

## 垃圾回收的并行与并发

### 并发

在操作系统中，是指一个时间段中有几个程序都处于已启动运行到运行完毕之间，且这几个程序都是在同一个处理器上运行。

并发不是真正意义上的“同时进行”，只是CPU把一个时间段划分成几个时间片段（时间区间），然后在这几个时间区间之间来回切换，由于CPU处理的速度非常快，只要时间间隔处理得当，即可让用户感觉是多个应用程序同时在进行。

![image-20200712202522051](img\image-20200712202522051.png)

### 并行

当系统有一个以上CPU时，当一个CPU执行一个进程时，另一个CPU可以执行另一个进程，两个进程互不抢占CPU资源，可以同时进行，我们称之为并行（Paralle1）。

其实决定并行的因素不是CPU的数量，而是CPU的核心数量，比如一个CPU多个核也可以并行。

适合科学计算，后台处理等弱交互场景

![image-20200712202822129](img\image-20200712202822129.png)

### 并发和并行对比

**并发**，指的是多个事情，在同一时间段内同时发生了。

**并行**，指的是多个事情，在同一时间点上同时发生了。

并发的多个任务之间是互相抢占资源的。并行的多个任务之间是不互相抢占资源的。

只有在多CPU或者一个CPU多核的情况中，才会发生并行。

否则，看似同时发生的事情，其实都是并发执行的。

### 垃圾回收的并行与并发

并发和并行，在谈论垃圾收集器的上下文语境中，它们可以解释如下：

- 并行（Paralle1）：<span style="color:red;">指多条垃圾收集线程并行工作</span>，但此时用户线程仍处于等待状态。如ParNew、Parallel Scavenge、Parallel old；

- 串行（Serial）
  - 相较于并行的概念，单线程执行。
  - 如果内存不够，则程序暂停，启动JM垃圾回收器进行垃圾回收。回收完，再启动程序的线程。

![image-20200712203607845](img\image-20200712203607845.png)

并发和并行，在谈论垃圾收集器的上下文语境中，它们可以解释如下：

<span style="color:red;">就是说在谈论垃圾收集器时，说到并发是指用户线程与垃圾回收线程可能是并行也可能时并发，而并行是指的垃圾回收器线程的并行</span>

并发（Concurrent）：指用户线程与垃圾收集线程同时执行（但不一定是并行的，可能会交替执行），垃圾回收线程在执行时不会停顿用户程序的运行。用户程序在继续运行，而垃圾收集程序线程运行于另一个CPU上；



>如：CMS、G1

![image-20200712203815517](img\image-20200712203815517.png)

## 安全点与安全区域

### 安全点

程序执行时并非在所有地方都能停顿下来开始GC，只有在特定的位置才能停顿下来开始GC，这些位置称为“安全点（Safepoint）”。

设置太少可能导致GC线程等待的时间太长，太多会导致频繁检查是否GC导致运行时的性能问题。大部分指令的执行时间都非常短暂，但是有一些执行时间比较长，运行非常快时间比较短的指令是很难暂停下来的，但是运行指令比较长慢的的动作是很容易停下来的，比如如方法调用、循环跳转和异常跳转等。所以选择一些执行时间较长的指令作为安全点，因为方法在执行过程中是非常快的，而方法调用时需要将方法入栈，是一个复杂的过程，所以会在方法执行前后设置为安全点。

如何在GC发生时，检查所有线程都跑到最近的安全点停顿下来呢？

- **抢先式中断**：（目前没有虚拟机采用了）首先中断所有线程。如果还有线程不在安全点，就恢复线程，让线程跑到安全点。
- **主动式中断**：（目前采用的方式）设置一个中断标志标识自己要GC了，各个线程运行到Safe Point的时候主动轮询这个标志，如果中断标志为真，则将自己进行中断挂起。（有轮询的机制）

### 安全区域

总结：就是为了防止线程遇到sleep或阻塞状态，那么jvm不会去一直等待线程执行完走到安全点，而是设置安全区域，在这个区域内保证对象的引用关系不会发生变化，区域中任何位置GC都是安全的，是安全点的一个扩展。在线程进入安全区域时jvm会做个标记，当线程运行到安全区域要结束时jvm会检查GC是否完成，如果没有完成就让线程等待执行

## 再谈引用

我们希望能描述这样一类对象：当内存空间还足够时，则能保留在内存中；如果内存空间在进行垃圾收集后还是很紧张，则可以抛弃这些对象。

【既偏门又非常高频的面试题】强引用、软引用、弱引用、虚引用有什么区别？具体使用场景是什么？
在JDK1.2版之后，Java对引用的概念进行了扩充，将引用分为：

- 强引用（Strong Reference）
- 软引用（Soft Reference）
- 弱引用（Weak Reference）
- 虚引用（Phantom Reference）

这4种引用强度依次逐渐减弱。除强引用外，其他3种引用均可以在java.1ang.ref包中找到它们的身影。如下图，显示了这3种引用类型对应的类，开发人员可以在应用程序中直接使用它们。

.![image-20200712205813321](img\image-20200712205813321.png)

Reference子类中只有终结器引用是包内可见（final），其他3种引用类型均为public，可以在应用程序中直接使用

==说明：下面的引用意思是 ，被引用的对象在堆中存在，引用是在栈或者堆中存在的一种连接关系，在GC时垃圾回收器会判断和堆中的对象的引用是什么类型的，根据引用类型不同，回收的时机不同==

- 强引用（StrongReference）：即类似“object obj=new Object（）”这种引用关系。无论任何情况下，==只要强引用关系还存在，垃圾收集器就永远不会回收掉被引用的对象==。
- 软引用（SoftReference）：在内存不够时进行回收，回收后还是不够就OOM，在内存不够GC时，发现堆中的对象只有一个软引用，就会进行回收。
- 弱引用（WeakReference）：在GC时候只要被发现就回收
- 虚引用（PhantomReference）：就是对堆中的对象没有引用，只是监视的作用==为一个对象设置虚引用关联的唯一目的就是能在这个对象被收集器回收时在队列中收到一个系统通知==。

#### 强引用

对象赋值给一个变量的时候，这个变量就成为指向该对象的一个强引用，垃圾收集器就永远不会回收掉强引用的对象。当变量超过了引用的作用域或者将引用赋值为nu11，这个对象可以当做垃圾被收集了，强引用是造成Java内存泄漏的主要原因之一。

```java
StringBuffer str = new StringBuffer("hello mogublog");
StringBuffer str1 = str;
```



那么我们将 str = null; 则 原来堆中的对象也不会被回收，因为还有其它对象指向该区域

#### 软引用

软引用是用来描述一些还有用，但非必需的对象。当内存足够时，不会回收这些对象。内存不够时，先回收不可达对象，然后还是不够会回收软引用的可达对象。

软引用通常用来实现缓存数据。如果还有空闲内存，就可以暂时保留缓存，当内存不足时清理掉。

弱引用和软引用都可以指定一个引用队列来监视对象被回收的通知。

```java
// 声明强引用
Object obj = new Object();
// 创建一个软引用
SoftReference<Object> sf = new SoftReference<>(obj);
obj = null; //销毁强引用，这是必须的，不然会存在强引用和软引用
```

#### 弱引用

> 发现即回收

在系统GC时，只要发现弱引用对象，不管系统堆空间使用是否充足，都会回收掉。所以弱引用关联的对象只能生存到下一次垃圾收集发生为止。弱引用和软引用都可以指定一个引用队列来监视对象被回收的通知。

软引用、弱引用都适合来保存那些可有可无的缓存数据。当系统内存不足时，这些缓存数据会被回收，不会导致内存溢出。

在JDK1.2版之后提供了WeakReference类来实现弱引用

```java
// 声明强引用
Object obj = new Object();
// 创建一个弱引用
WeakReference<Object> sf = new WeakReference<>(obj);
obj = null; //销毁强引用，这是必须的，不然会存在强引用和弱引用
```

面试题：你开发中使用过WeakHashMap吗？

WeakHashMap用来存储图片信息，可以在内存不足的时候，及时回收，避免了OOM

#### 虚引用

当一个对象只剩下一个虚引用时，就等于没有引用，会在GC时被回收

虚引用不能单独使用，也无法通过虚引用来获取被引用的对象。当试图通过虚引用的get（）方法取得对象时，总是null

设置虚引用目的在于跟踪垃圾回收过程。比如：能在这个对象被收集器回收时收到一个队列通知。

虚引用必须和引用队列一起使用。虚引用在创建时必须提供一个引用队列作为参数。当垃圾回收器准备回收一个对象时，如果发现它还有虚引用，就会在回收对象后，将这个虚引用加入引用队列，以通知应用程序对象的回收情况。可以将一些资源释放操作放置在虚引用中执行和记录。

存在的意义：用来监控我们堆外内存的对象，因为jvm管理不到堆外内存，比如网络读取数据时一般会将数据放到堆外内存，所以可以使用虚引用可以检测到堆外内存对象是否被回收。

> 虚引用无法获取到我们的数据

```java
// 声明强引用
Object obj = new Object();
// 声明引用队列
ReferenceQueue phantomQueue = new ReferenceQueue();
// 声明虚引用（还需要传入引用队列）
PhantomReference<Object> sf = new PhantomReference<>(obj, phantomQueue);
obj = null; 
```

#### 终结器引用

它用于实现对象的finalize() 方法，也可以称为终结器引用

无需手动编码，其内部配合引用队列使用

在GC时，终结器引用入队。由Finalizer线程通过终结器引用找到被引用对象调用它的finalize()方法，第二次GC时才回收被引用的对象

# 六、垃圾回收器

### 垃圾收集器分类

#### 按线程数分

**按线程数分**（垃圾回收线程数），可以分为串行垃圾回收器和并行垃圾回收器。

![image-20200713083030867](img\image-20200713083030867.png)

串行回收指的是在同一时间段内只允许有一个CPU用于执行垃圾回收操作，此时工作线程被暂停，直至垃圾收集工作结束。

- 在诸如单CPU处理器或者较小的应用内存等硬件平台不是特别优越的场合，串行回收器的性能表现可以超过并行回收器和并发回收器。所以，串行回收默认被应用在客户端的Client模式下的JVM中
- 在并发能力比较强的CPU上，并行回收器产生的停顿时间要短于串行回收器。

和串行回收相反，并行收集可以运用多个CPU同时执行垃圾回收，因此提升了应用的吞吐量，不过并行回收仍然与串行回收一样，采用独占式，使用了“stop-the-world”机制。

#### 按工作模式分

按照工作模式分，可以分为并发式垃圾回收器和独占式垃圾回收器。

- 并发式垃圾回收器与应用程序线程交替工作，以尽可能减少应用程序的停顿时间。
- 独占式垃圾回收器（Stop the world）一旦运行，就停止应用程序中的所有用户线程，直到垃圾回收过程完全结束。

![image-20200713083443486](img\image-20200713083443486.png)

#### 按碎片处理方式分

按碎片处理方式分，分为压缩式垃圾回收器和非压缩式垃圾回收器。

- 压缩式：在回收完成后对存活对象进行压缩整，消除回收后的碎片。
- 非压缩式的垃圾回收器不进行这步操作。

#### 按工作的内存区间分，又可分为年轻代垃圾回收器和老年代垃圾回收器。

### 评估GC的性能指标

主要关注前三个：

- **==吞吐量==**：运行用户代码的时间占总运行时间的比例（总运行时间 = 程序的运行时间 + 内存回收的时间）
- ==**暂停时间**==：执行垃圾收集时，程序的工作线程被单次暂停的时间。
- **==内存占用==**：Java堆区所占的内存大小。
- **收集频率**：相对于应用程序的执行，收集操作发生的频率。频率高了暂停时间短一点，频率低了吞吐量好一点（切换消耗）。
- **垃圾收集开销**：吞吐量的补数，垃圾收集所用时间与总运行时间的比例。
- **快速**：一个对象从诞生到被回收所经历的时间。

因为现在的硬件发展的很快基本也够用所以不用特别关注。主要关注就是吞吐量和暂停时间，而内存的扩大也增加了暂停时间。

内存小就会暂停时间小，并且收集频率高，并且切换线程也消耗时间，吞吐量低，用户线程工作效率低，内存大就会频率低，暂停时间长，吞吐量高，用户工作线程效率高

应用程序能容忍较高的暂停时间，因此，高吞吐量的应用程序有更长的时间基准，快速响应是不必考虑的

### 吞吐量vs暂停时间

高吞吐量较好因为这会让应用程序的最终用户感觉只有应用程序线程在做“生产性”工作。直觉上，吞吐量越高程序运行越快。

低暂停时间（低延迟）较好因为从最终用户的角度来看不管是GC还是其他原因导致一个应用被挂起始终是不好的。这取决于应用程序的类型，有时候甚至短暂的200毫秒暂停都可能打断终端用户体验。因此，具有低的较大暂停时间是非常重要的，特别是对于一个交互式应用程序。

不幸的是”高吞吐量”和”低暂停时间”是一对相互竞争的目标（矛盾）。

因为如果选择以吞吐量优先，那么必然需要降低内存回收的执行频率，但是这样会导致GC需要更长的暂停时间来执行内存回收。

相反的，如果选择以低延迟优先为原则，那么为了降低每次执行内存回收时的暂停时间，也只能频繁地执行内存回收，但这又引起了年轻代内存的缩减和导致程序吞吐量的下降。

在设计（或使用）GC算法时，我们必须确定我们的目标：一个GC算法只可能针对两个目标之一，或尝试找到一个二者的折衷。

现在标准：**在最大吞吐量优先的情况下，降低停顿时间**

# 不可达对象一定被回收吗？

不一定，如果对象重写了finlaze方法，会在对象被回收前进行调用，可以在这个方法将对象进行复活，如果，没有复活就会被回收

### 7种经典的垃圾收集器

- 串行回收器：Serial、Serial old
- 并行回收器：ParNew、Parallel Scavenge、Parallel old
- 并发回收器：CMS、G11

![image-20200713093551365](img\image-20200713093551365.png)

### 垃圾收集器的组合关系

新生代收集器：Serial、ParNew、Paralle1 Scavenge；

老年代收集器：Serial old、Parallel old、CMS；

整堆收集器：G1；

![image-20200713094745366](img/image-20200713094745366.png)

- 新生代都是复制算法，老年代cms是标清，其他两个是标整，G1特殊
- 两个收集器间有连线，表明它们可以搭配使用：Serial/Serial old、Serial/CMS、ParNew/Serial old、ParNew/CMS、Parallel Scavenge/Serial 0ld、Parallel Scavenge/Parallel old、G1；
- 其中Serial old作为CMs出现"Concurrent Mode Failure"失败的后备预案。因为cms使用的是标记清除，所以会产生内存碎片，最后会产生fullGC也可能用户线程需要的内存速度大于垃圾收集的速度而暂停用户线程使用Serial old
- （红色线）在JDK9中完全取消了Serial+CMS、ParNew+Serial old
- （绿色线）JDK14中：弃用Paralle1 Scavenge和Serialold GC组合（JEP366）
- （青色线）JDK14中：删除CMs垃圾回收器（JEP363）

# ==垃圾收集器总结==

并行和串行都会暂停用户线程

### Serial

作用于是一个新生代串行垃圾收集器，采用复制算法，老年代可以和serial old（标整算法）和CMS+serial old（标清）配合使用，在jdk9中删除了与CMS的组合使用。serial适合在单核cpu场景下使用。设置新生代Serial，老年代默认使用Serial old

### ParNew

是一个新生代并行的垃圾收集器，采用复制算法，可以和serial old（标整算法）和CMS+serial old（标清）配合使用，在jdk9中删除了和CMS的组合使用。

断定ParNew收集器的效率在任何场景下都会比serial收集器更高效？

ParNew在多CPU的可以充分利用物理硬件，提升程序的吞吐量

单个CPUserial 收集器更高效，由于CPU不需要频繁地做任务切换，可以避免多线程切换过程中的额外开销。

jdk9后只有ParNew GC能与CMS收集器配合工作

可以设置+UseParNewGC指定使用ParNew，不影响老年代。

### Parallel scavenge回收器：吞吐量优先

作用于新生代吞吐量优先的并行垃圾回收器，采用复制算法，可以和serial old（标整算法）与Parallel old配合使用，在jdk14时候弃用serial old，但是没有删除，还可以继续使用。和ParNew的最大区别就是Parallel scavenge可以控制吞吐量与暂停时间，并且Paralle1 Scavenge可以自适应调节策略调整年轻代的大小、Eden和Survivor的比例、晋升老年代的对象年龄等参数，已达到在堆大小、吞吐量和停顿时间之间的平衡点。可以手动设置吞吐量时间和暂停时间。比ParNew更强大。

### CMS回收器：低延迟

作用于老年代的低延迟的并发收集器，采用==标记清理==算法，可以配合新生代的seria和ParNew配合使用，并且把serial old作为应急收集器，并且因为垃圾收集线程和用户线程是并行的，所以为了保证用户线程有可用的内存，需要cms提前进行垃圾回收，由于最耗费时间的并发标记与并发清除阶段都不需要暂停工作，所以整体的回收是低停顿的。

![](img/image-20200713205154007.png)

整个过程分为4个主要阶段(涉及STW的阶段主要是：初始标记 和 重新标记)

- **初始标记**（Initial-Mark）阶段：==先“stop-the-world”暂停用户线程，然后只是**标记GCRoots直接关联的对象**。由于直接关联对象比较小，所以这里的速度非常快。==
- **并发标记**（Concurrent-Mark）阶段：==从Gc Roots的直接关联对象开始遍历整个对象图的过程，这个过程耗时较长但是不需要停顿用户线程，可以与垃圾收集线程一起并发运行。==
- **重新标记**（Remark）阶段：==主要就是修正二阶段的并发标记过程的错误或者未标记的垃圾，因为二阶段是垃圾回收线程与用户线程并发执行，所以会产生一些误标或漏标==
- **并发清除**（Concurrent-Sweep）阶段：==不需要移动存活对象，直接清理垃圾，不会暂停用户线程，所以很快==

CMS收集器不能像其他收集器那样等到老年代几乎完全被填满了再进行收集，而是当堆内存使用率达到某一阈值时，便开始进行回收，以确保应用程序在CMS工作过程中依然有足够的空间支持应用程序运行。要是CMS运行期间预留的内存无法满足程序需要，就会出现一次“Concurrent Mode Failure”失败，这时虚拟机将启动后备预案：临时启用Serial old（进行标记整理）收集器来重新进行老年代的垃圾收集，这样停顿时间就很长了。

CMS收集器的垃圾收集算法采用的是**标记清除算法**，这意味着每次执行完内存回收后，由于被执行内存回收的无用对象所占用的内存空间极有可能是不连续的一些内存块，不可避免地将会产生一些内存碎片。那么CMS在为新对象分配内存空间时，将无法使用指针碰撞（Bump the Pointer）技术，而只能够选择空闲列表（Free List）执行内存分配。

### CMS为什么不使用标记整理算法？

==因为清除阶段是和用户线程并行执行的，整理对象会影响用户线程对对象的使用==

### CMS缺点

- 会产生内存碎片，导致并发清除后，用户线程可用的空间不足。在无法分配大对象的情况下，不得不提前触发FullGC。
- 在并发阶段，它虽然不会导致用户停顿，但是会因为占用了一部分线程而导致应用程序变慢，总吞吐量会降低。
- CMS收集器无法处理浮动垃圾。如果在并发标记阶段产生GCroot对象将无法进行标记。只能在下次进行标记

### 设置的参数

==主要可以设置并发收集线程数、cms堆内垃圾回收阈值、多少次fullGC后整理内存碎片==

### 小结

==HotSpot有这么多的垃圾回收器，那么如果有人问，Serial GC、Parallel GC、Concurrent Mark Sweep GC这三个Gc有什么不同呢？==

请记住以下口令：

- 如果你想要最小化地使用内存和并行开销，请选Serial GC；
- 如果你想要最大化应用程序的吞吐量，请选Parallel GC；
- 如果你想要最小化GC的中断或停顿时间，请选CMs GC。

### CMS在jdk的变化

JDK14新特性：删除CMs垃圾回收器（JEP363）移除了CMS垃圾收集器，如果在JDK14中使用
XX：+UseConcMarkSweepGC的话，JVM不会报错，只是给出一个warning信息，但是不会exit。JVM会自动回退以默认GC方式启动JVM

# TODOparallel也可以吞吐量和暂停时间兼顾吧

## G1回收器：区域化分代式

G1：是一个区域化分代式的垃圾回收器，兼顾新生老年代和吞吐量和停顿时间+G1中region角色+根据回收时间来优先级回收垃圾（维护一个空闲列表）

G1实在jdk7中新引入的一个全功能的收集器，因为可以做到控制低延迟的情况下尽可能的提高用户线程吞吐量，并且可以同时收集新生代和老年代。

G1适用于多核cpu和大内存的服务器应用GC收集器，在尽量满足停顿时间的同时保证了吞吐量，即立即响应

之所以可以同时满足这两个要求的原因就是G1使用Region（区域）将物理上连续的内存划分为多个不连续的Region来分别表示eden，幸存者区、old，并且增加了Humongous用于存储短期存在的大对象

G1避免在堆中进行全区域的垃圾收集。G1根据回收性价比（就是每个region的回收时间与垃圾大小）会在后台维护一个回收优先列表，每次根据有限的回收时间来按照优先列表回收。



### G1优点

回收时可以并行GC收集阻塞用户线程，也同时拥有并发与用户线程同时执行，

同时兼顾老年新生代，

回收时分代回收，但是结构上没有严格的连续分代。

并且复制标记清除整理算法兼备，因为是先标记Region中的分段垃圾然后进行复制到其他Region，最后清除这个旧的Region

==因为垃圾收集线程优先级比较低，所以在G1垃圾回收线程速度比较低时应用线程会帮助垃圾回收过程==

### G1缺点

G1垃圾收集产生的内存占用和运行时的额外执行的消耗比CMS高。主要是G1使用的这种复制整理算法

小内存CMS更优，大内存G1更优，平衡点在6-8GB之间。

## G1与Cms的区别

首先G1可以新生代老年代兼顾

### G1可以设置哪些

每个Region大小、GC最大停顿时间（尽可能完成，不保证），触发GC阈值、并行收集线程数与并发标记线程数

### G1使用流程：

- 第一步：开启G1垃圾收集器
- 第二步：设置堆的最大内存
- 第三步：设置最大的停顿时间

### G1中Region的角色：

eden、幸存者区、老年区、大对象区、空白区

### G1回收过程

- 年轻代GC（Young GC）
- 老年代并发标记过程（Concurrent Marking）
- 混合回收（Mixed GC）

（如果需要，单线程、独占式、高强度的Fu11GC还是继续存在的。它针对GC的评估失败提供了一种失败保护机制，即强力回收。）

分配内存时当年轻代的Eden区用尽时开始年轻代回收过程；年轻代收集阶段是并行的独占式收集器。从年轻代区间移动存活对象到Survivor区间或者老年区间。

当堆内存使用达到一定值（默认45%）时，开始老年代并发标记过程。

标记完成马上开始混合回收过程。G1的老年代回收器不需要整个老年代被回收，一次只需要扫描/回收一小部分老年代的Region就可以了。同时，这个老年代Region是和年轻代一起被回收的。

### G1回收过程中扫描对象存活问题

一个Region中的对象可能被其他任意Region中对象引用，判断对象存活时，是否需要扫描整个Java堆才能保证准确？

回收新生代也不得不同时扫描老年代？这样的话会降低MinorGC的效率；

每个Region都有一个记忆集，记忆集保存了当前Region中每个对象被其他Region引用情况。

### G1回收过程

- 第一阶段，扫描根

根是指static变量指向的对象，正在执行的方法调用链条上的局部变量等。根引用连同RSet记录的外部引用作为扫描存活对象的入口。

- 第二阶段，更新RSet

处理dirty card queue（见备注）中的card，更新RSet。此阶段完成后，RSet可以准确的反映老年代对所在的内存分段中对象的引用。

- 第三阶段，处理RSet

识别被老年代对象指向的Eden中的对象，这些被指向的Eden中的对象被认为是存活的对象。

- 第四阶段，复制对象。

此阶段，对象树被遍历，Eden区内存段中存活的对象会被复制到Survivor区中空的内存分段，Survivor区内存段中存活的对象如果年龄未达阈值，年龄会加1，达到阀值会被会被复制到o1d区中空的内存分段。如果Survivor空间不够，Eden空间的部分数据会直接晋升到老年代空间。

- 第五阶段，处理引用

处理Soft，Weak，Phantom，Final，JNI Weak 等引用。最终Eden空间的数据为空，GC停止工作，而目标内存中的对象都是连续存储的，没有碎片，所以复制过程可以达到内存整理的效果，减少碎片。

### G1老年代回收过程-并发标记过程

- 初始标记阶段：标记从根节点直接可达的对象。这个阶段是sTw的，并且会触发一次年轻代GC。
- 根区域扫描（Root Region Scanning）：G1 Gc扫描survivor区直接可达的老年代区域对象，并标记被引用的对象。这一过程必须在youngGC之前完成。
- 并发标记（Concurrent Marking）：在整个堆中进行并发标记（和应用程序并发执行），此过程可能被youngGC中断。在并发标记阶段，若发现区域对象中的所有对象都是垃圾，那这个区域会被立即回收。同时，并发标记过程中，会计算每个区域的对象活性（区域中存活对象的比例）。
- 再次标记（Remark）：由于应用程序持续进行，需要修正上一次的标记结果。是STW的。G1中采用了比CMS更快的初始快照算法：snapshot-at-the-beginning（SATB）。
- 独占清理（cleanup，STW）：计算各个区域的存活对象和GC回收比例，并进行排序，识别可以混合回收的区域。为下阶段做铺垫。是sTw的。这个阶段并不会实际上去做垃圾的收集
- 并发清理阶段：识别并清理完全空闲的区域。

### G1回收过程 - 混合回收

当越来越多的对象晋升到老年代o1d region时，为了避免堆内存被耗尽，虚拟机会触发一个混合的垃圾收集器，即Mixed GC，该算法并不是一个old GC，除了回收整个Young Region，还会回收一部分的old Region。这里需要注意：**是一部分老年代，而不是全部老年代**。可以选择哪些o1d Region进行收集，从而可以对垃圾回收的耗时时间进行控制。也要注意的是Mixed GC并不是Full GC。

并发标记结束以后，老年代中百分百为垃圾的内存分段被回收了，部分为垃圾的内存分段被计算了出来。默认情况下，这些老年代的内存分段会分8次（可以通过-XX:G1MixedGCCountTarget设置）被回收

混合回收的回收集（Collection Set）包括八分之一的老年代内存分段，Eden区内存分段，Survivor区内存分段。混合回收的算法和年轻代回收的算法完全一样，只是回收集多了老年代的内存分段。具体过程请参考上面的年轻代回收过程。

由于老年代中的内存分段默认分8次回收，G1会优先回收垃圾多的内存分段。垃圾占内存分段比例越高的，越会被先回收。并且有一个阈值会决定内存分段是否被回收，

XX:G1MixedGCLiveThresholdPercent，默认为65%，意思是垃圾占内存分段比例要达到65%才会被回收。如果垃圾占比太低，意味着存活的对象占比高，在复制的时候会花费更多的时间。

混合回收并不一定要进行8次。有一个阈值-XX:G1HeapWastePercent，默认值为1e%，意思是允许整个堆内存中有10%的空间被浪费，意味着如果发现可以回收的垃圾占堆内存的比例低于1e%，则不再进行混合回收。因为GC会花费很多的时间但是回收到的内存却很少。

### G1回收可选的过程4 - Full GC

G1的初衷就是要避免Fu11GC的出现。但是如果上述方式不能正常工作，G1会停止应用程序的执行（stop-The-world），使用单线程的内存回收算法进行垃圾回收，性能会非常差，应用程序停顿时间会很长。

要避免Fu11GC的发生，一旦发生需要进行调整。什么时候会发生Ful1GC呢？比如堆内存太小，当G1在复制存活对象的时候没有空的内存分段可用，则会回退到ful1gc，这种情况可以通过增大内存解决。
导致61Fu11GC的原因可能有两个：

- EVacuation的时候没有足够的to-space来存放晋升的对象；
- 并发处理过程完成之前空间耗尽。