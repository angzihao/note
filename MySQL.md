# 一、基础查询

1. 使用concat拼接查询：**concat参数可以是字段名称也可以是常量**

~~~sql
select concat（first_name,last_name） as "姓名" from user//两个参数
select concat（first_name,"_",last_name） as "姓名" from user//三个参数
~~~

2. sql中的“+”符号的使用

   **作用：做加法运算**    

    select 数值+数值; 直接运算     SELECT 1+10 \=====>11;

    select 字符+数值;先试图将字符转换成数值，如果转换成功，则继续运算；否则转换成0， 再做运算    SELECT "a"+10 \=====>10;

   select null+值;结果都为null	SELECT null+10 \=====>null;

3. 去重操作：distinct与GROUP BY都可以实现去重操作，但是都只是对单列去重

   ~~~sql
   SELECT distinct age，password  from user ;//会对age+password的数据去重
   SELECT  age ，password from user GROUP BY age,`password`;//会对age+password的数据去重
   SELECT distinct age  from user ;
   SELECT  age  from user GROUP BY age;
   ~~~

4. 关系运算符

   <	>	<=	>=	=(<=>)	<>(!=)		不建议使用!=

5. 逻辑查询

   and（&&且）or（||或）not（！非）建议使用字母

6. 模糊查询

   like/not like		'_'匹配单个字符，%匹配多个字符

   ~~~sql
   SELECT * FROM user where nickname like "%\_%"//如果想要匹配'_'就进行转义
   ~~~

7. IFNULL判断是否为空

   ~~~sql
   SELECT ifnull(age,"数据空")  from user ;//查询age，如果为空就替换成"数据空"
   SELECT 底薪, sum（底薪+ifnull(提成,0)）  as 总工资 from user;//计算总工资，如果提成为0就替换成0
   ~~~

8. is null | is not null查询数据为空或者不为空

   ~~~JAVA
   SELECT * from user where age is null;//查询age为空的数据
   ~~~

9. =、is null、<=>的区别

   ![](img/842388468238762.png)

# 二、排序字段

asc：升序，默认升序

desc：降序

语法：order by 字段名 asc/desc

# 三、常用函数

### 字符串函数

1. concat：拼接，首行案列
2. upper/lower：大小写转换
3. substr：截取字符串（sql中的索引都是从1开始算）
4. instr：查找子串第一次出现的索引，没有就是0，格式：instr（“”）
5. trim：去除收尾空格或去除指定字符串，格式：trim（“需要去除的指定字符串，不写就是去除空格”，操作的字符串）
6. lpad/rpad：左右填充，格式lpad（字段，希望字符长度，填充字符串），说明，如果字段长度大于希望字符长度，就会是截取
7. replace：替换，格式replace（字段，原字符串，目标字符串）

### 数学函数

1. round：四舍五入	案例：SELECT ROUND(1.32424,3)四舍五入保留3位
2. ceil/floor：向上/下取整     
3. truncate：直接截断      SELECT TRUNCATE(1.3453,2)
4. mod：取余   SELECT MOD(10,-3)//1

### 日期函数

1. SELECT NOW()：返回当前日期+时间
2. curdate/curtime：返回当前的日期/时间
3. datediff：求两个日期相差多少天

### 分组函数

1. 分类
   + sum
   + max
   + min
   + count
   + avg
2. sum和avg用于处理数字，否则位0
3. min、max、count可以处理任何类型
4. 分组函数都忽略null值
5. 都可以使用distinct实现去重操作 select sum（distinct age）
6. count（\*）与count（1）统计结果集的行数，count（字段）统计非空的个数
7. 效率上MyISAM,COunt(*)最高
8. InnoDB上count（\*）与count（1）>count（字段）
9. 可以根据多个字段分组，比如age与sex，只有age与sex都相同的值才会被认为是一组

## TODOcount（\*）与count（1）>count（字段）为什么







**分组函数使用说明：和分组函数一起查询的字段需要使用GROUP BY进行分组，因为不使用GROUP BY进行分组默认是所有的数据都是一组，使用分组函数后就只会有一个数据，分组函数的结果是有几个组就有几个结果。如果和分组函数一起查询的字段没有使用GROUP BY进行分组，那么就会有一行分组结果和很多行字段查询结果，就会行不匹配，分组结果不属于任何一行的字段查询结果，结果就是查询字段就有第一个数据被查出来。**

举例：SELECT SUM(age) from user查询的是所有的age的和，如果加上分组SELECT SUM(age) from user  GROUP BY sex，查询的就是男的age的和和女的age的和。

# 四、having与where

1. 筛选表中存在的字段使用where，筛选分组后的结果集使用having，having就是对查询结果集的再次筛选
2. 如果字段既能分组前筛选也能分组后筛选，尽量使用分组前筛选，提高效率

![](img/having.png)



# 五、多表连接查询

1. 多表连接查询可能导致笛卡尔积的出现：就是a表有10行数据，b表有10行数据，查询出来结果就是10*10=100

2. 发生原因：没有有效的查询条件，解决办法就是添加有效查询条件

3. 按年代分类

   sql92标准：仅支持内连接（等值、非等值、自连接）

   ​	语法就直接使用where进行条件连接

   sql99语法：支持内连接、外连接（左外和右外）、交叉连接

   ~~~sql
   select user.*,branch.* from user [连接类型inner/outer] join branch on [连接条件] where筛选条件等
   ~~~

   

### 内连接

连接条件有：等值连接、非等值连接、自连接

内连接：就是只有符合连接条件的数据才会显示，下面就是boy中有girls_id值的会显示，null的                        不显示

92语法内连接

~~~sql
SELECT boy.*,girls.* from boy,girls where boy.girls_id=girls.id//等值连接
SELECT user.*,ageleave.* from user,ageleave where user.age between ageleave.minage and maxage//非等值连接
SELECT user1.*,user2.name from user user1,user user2 where user1.jobid=user2.jobid//自连接，就是自己连接自己
~~~

99语法内连接

~~~sql
select user.*,branch.* from user inner join branch on [连接条件] where筛选条件等
~~~

### 外连接

外连接：就是需要有一个主表，结果中必须要包含主表所有数据行，如果副表中没有记录与主表记录相匹配就显示空值

外连接分为：左外连接、右外连接、全外连接

一般左连接实现的查询交换表的顺序后右连接也可以查询

+ 左外连接：将左边的作为主表对右表进行查询，没有则补null

  ~~~sql
  select user.*,branch.* from user left outer join branch on [连接条件] where筛选条件等
  ~~~

+ 右外连接：将右边的作为主表对左表进行查询，没有则补null

  ~~~java
  select user.*,branch.* from user right outer join out branch on [连接条件] where筛选条件等
  ~~~

+ 全外连接：两个表都作为主表，没有则补null

  ~~~sql
  select user.*,branch.* from user full outer join out branch on [连接条件] where筛选条件等
  ~~~

### 交叉连接也叫笛卡尔积（MySQL不支持）

# 五、子查询/内查询

1. 嵌套在其他语句内部的select语句称为子查询或内查询，外面的语句可以是insert、update、delete、select等，一般select作为外面语句较多

2. 按查询结果集的行列数不同分为：

   + 标量子查询：结果集只有一行一列
   + 列子查询：结果集有一列多行
   + 行子查询：结果集有一行多列
   + 表子查询：结果集有多行多列就是一个表

3. 子查询可以出现的位置

   + select 后：仅仅支持标量子查询
   + from后 ：支持表子查询
   + where和having支持：标量子查询、列子查询、行子查询
   + exists（相关查询，就是判断是否存在）支持：表子查询

4. 可以使用的操作符有

   标量子查询与行子查询：<   >  >=   <=   =   <>不等于

   列子查询与表子查询：

   ​	in/not in：存在或不存在	

   ​	any/some：和返回的结果中任意一个值比较   一般配合<  >   <>   比如：age<any(select age from user)

   ​	all：和返回的结果中所有值比较		一般配合<  >   <>

# 六、分页查询

格式为： limit [offset] size

​			 offset：是起始索引，默认从第0条开始，可以省略不写：**索引值是从第0条开始（所以从第几条开始要-1）**

​			 size：查询记录数

# 七、联合查询union

使用格式： 可以将多个表的查询结果合并到一个结果

~~~sql
select * from user1 union select * from user2 union select * from user3
~~~

要求：需要多条查询语句的结果的列数相等，最好每个查询语句查询的每一个列相互对应，否则显示的结果信息将会不对应

特点：union默认去重，可以使用union all进行不去重

# 八、事务

1. 事务是一条或多条sql语句组成的一个执行单位，一组sql要么都执行，要么都不执行

2. 事务的特点：ACID

   + A：原子性，一个事务的sql要么都执行要么都失败
   + C：一致性，事务可以将 数据从一个状态修改为另一个状态
   + I：隔离性， 一个事务不受其他事务的干扰而成功或失败
   + D：持久性，事务一旦提交数据将被永久的持久化在本地

3. 事务的类型

   + 显示事务：关闭自动事务提交功能，手动开启事务，手动提交
   + 隐式事务：delete update insert语句执行时默认使用的隐式事务

4. 手动关闭事务：set autocommit=0

5. 手动开启事务：start transaction

6. 提交事务：commit;

7. 回滚事务：rollback

8. 事务可能出现的问题：

   + 脏读：读取到其他事务没有提交的数据
   + 幻读：在一次事务中对一个字段的行数多次读取到不同的值
   + 不可重复读：在一次事务中对同一个数据读取到不同的值

9. 事务的隔离级别

   + 读未提交
   + 读已提交（oracle默认隔离级别）
   + 可重复读（mysql默认隔离级别）
   + 串行化

   oracle仅支持读已提交和串行化，mysql都支持

# 九、存储过程和函数

1. 存储过程和函数的区别：

   存储过程可以有0个或多个返回结果

   函数只能有一个结果返回

2. 都是处理一组预编译好的一组sql语句的集合，理解成批处理语句

3. 有点：

   + 提高代码的重用性
   + 简化重复操作
   + 减少了编译次数和数据库服务连接的次数

4. 存储过程创建语法

   ~~~sql
   create procedure 存储过程名（参数列表）
   begin
   	sql集合
   end
   ~~~

   参数声明规则：模式 参数名 参数类型

   参数模式：in只作为输入，out只作为输出，inout作为输入输出

   调用语法

   ~~~sql
   call 存储过程名（参数列表）
   ~~~

5. 函数创建语法

   ~~~sql
   create function 函数名（参数列表）returns 返回值类型
   begin
   	函数体
   	return 值；
   end
   ~~~

   函数参数格式：参数名  参数类型

   调用函数语法：select 函数名

   函数中的return可以不放在最后一条语句

# 十、DML数据操作语言

1. 插入语法

   + 方式一，隐式插入：列名需要与值相对应

     ~~~sql
     insert into 表名（列名，列名）values（值1，值2），（值1，值2）。。。。
     ~~~

   + 方式二，显示插入，不写列名，默认插入所有的列（按列的顺序），为null的列需要显示指定为null

     ~~~sql
     insert into 表名 values（值1，值2，null），（值1，值2）。。。
     ~~~

   + 方式三

     ~~~sql
     insert into 表名 set 列名=值，列名=值
     ~~~

     区别：方式一二可以一次插入多行

2. 修改语法

   修改单表记录为

   ~~~sql
   update 表名 set 列名=值，列名=值 where 条件
   ~~~

   修改多表记录

   92语法

   ~~~sql
   update user1，user2 set 列名=值，列名=值 where 连接条件 and筛选条件
   ~~~

   99语法

   ~~~sql
   update user1 left join user2 on 连接条件 set 列名=值，，列名=值 where 筛选条件
   ~~~

3. 删除数据

   单表删除

   ~~~sql
   delete from user where 条件
   ~~~

   多表删除

   92语法

   ~~~sql
   delete user1，user2 from user1，user2  where 连接条件 and筛选条件
   ~~~

   99语法

   ~~~sql
   delete user1，user2 from user1 left join user2 on 连接条件 set 列名=值，，列名=值 where 筛选条件
   ~~~

4. **truncate（创k特）清空**，语法 truncate table  表名

   delete与truncate区别

   + delete可以使用where条件truncate不可以
   + truncate效率稍微高点
   + truncate没有返回值，delete有
   + 使用truncate删除数据后自增长的列会从新计算，delete会继续
   + truncate不能回滚数据，delete可以回滚

# DDL数据定义语言

1. 使用的关键字

   + 创建：create
   + 修改：alter
   + 删除：drop

2. 创建库或表

   ~~~sql
   create database if exists books
   create table 表名 (列名 类型 【长度 约束】，列名 类型 【长度 约束】，列名 类型 【长度 约束】)
   ~~~

3. 修改表

   ~~~sql
   alter table 表名 add|drop|modify|change column 列名 类型 【first|after 字段名】
   ~~~

4. 删除库或表

   ~~~sql
   drop database if exists books
   drop  table if exists  表名
   ~~~

5. 表复制

   ~~~sql
   create table user like user2//仅仅复制表结构
   create table user select * from user2 //复制表结构和数据
   ~~~

# 十一、数据类型

### 数值型

![](img/Snipaste_2021-02-23_16-33-26.png)

`DECIMAL(P，D)`表示列可以存储`D`位小数的`P`位数。十进制列的实际范围取决于精度和刻度。

### 日期类型

![](img/Snipaste_2021-02-23_16-34-33.png)

datetime与timestamp的区别：

+ datetime占8个字节timestamp占4个字节
+ datetime存储时间比timestamp广
+ timestamp可以设置时区动态查询时间datetime不可以（set time_zone='+9:00'）

### 字符串类型

![](img/Snipaste_2021-02-23_16-35-18.png)

# 十二、约束

![](img/1231223132132432.png)

主键和唯一的区别：

+ 一个表只能有一个主键
+ 主键不能为null，唯一可以

自增长列：auto_increment_increment默认从1开始步长为1，可以手动设置

# 十三、视图

1. 视图是一种虚拟表，和普通表一样使用

2. 视图和表的区别是视图只保存了sql的逻辑，而数据实际还是在表中存储，**视图可以使用DML数据操作语言来修改表中数据，但是不推荐使用**，一般不使用视图增删改查数据

3. 创建视图语法

   ~~~sql
   create view 视图名 as select * from user
   ~~~

   修改

   ~~~sql
   create or replace view 视图名 as select * from user
   alert view 视图名 as select * from user
   ~~~

   删除

   ~~~sql
   drop view 视图名
   ~~~

   

# 十三、DCL用户管理语言

# 十四、触发器

触发器是与表有关的数据库对象，指在insertlupdate/delete之前或之后，触发并执行触发器中定义的SQL语句集合。触发器的这种特性可以协助应用在数据库端确保数据的完整性，日志记录，数据校验等操作。
使用别名OLD和NEW来引用触发器中发生变化的记录内容，这与其他的数据库是相似的。现在触发器还只支持行级触发，不支持语句级触发。

语法：

![img](img\23432423421541556465.png)

![image-20210224214550809](img\image-20210224214550809.png)

![](img/Snipaste_2021-02-24_21-46-20.png)